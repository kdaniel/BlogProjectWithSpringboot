package com.danielkocsis.springbootproject.service;

import com.danielkocsis.springbootproject.domain.Story;
import com.danielkocsis.springbootproject.repository.BloggerRepository;
import com.danielkocsis.springbootproject.repository.StoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StoryService {

    private StoryRepository storyRepository;
    private BloggerRepository bloggerRepository;

    @Autowired
    public void setBloggerRepository(BloggerRepository bloggerRepository) {
        this.bloggerRepository = bloggerRepository;
    }

    @Autowired
    public void setStoryRepository(StoryRepository storyRepository) {
        this.storyRepository = storyRepository;
    }

    public List<Story> getStories() {

        return storyRepository.findAll();
    }

    public Story getStory() {
        return storyRepository.findFirstByOrderByPostedDesc();
    }

    public Story getSpecificStory(String title) {
        return storyRepository.findByTitle(title);
        // itt kene lekezelni ha olyan storit ker a frontend ami nincs
    }

//    @PostConstruct //after creating the server this runs once
//    public void init() {
//        // create new blogger
//        Blogger blogger = new Blogger("FromCode", 1);
//        // save to blogger repo
//        bloggerRepository.save(blogger);
//        //create story with the blogger
//        Story story = new Story("codeTitle", "codeContent", new Date(), blogger);
//        //save story to story repo
//        storyRepository.save(story);
//    }
}
