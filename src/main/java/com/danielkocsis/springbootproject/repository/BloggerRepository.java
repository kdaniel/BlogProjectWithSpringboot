package com.danielkocsis.springbootproject.repository;

import com.danielkocsis.springbootproject.domain.Blogger;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface BloggerRepository extends CrudRepository<Blogger, Long>{
    // we change the return type to
    List<Blogger> findAll();
}

