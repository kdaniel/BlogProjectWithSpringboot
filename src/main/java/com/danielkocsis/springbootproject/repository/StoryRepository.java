package com.danielkocsis.springbootproject.repository;

import com.danielkocsis.springbootproject.domain.Story;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface StoryRepository extends CrudRepository<Story, Long>{
    // we change the return type to
    List<Story> findAll();
    Story findFirstByOrderByPostedDesc();

    Story findByTitle(String title);
}