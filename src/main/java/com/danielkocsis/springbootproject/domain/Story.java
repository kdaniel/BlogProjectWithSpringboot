package com.danielkocsis.springbootproject.domain;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "Stories")
public class Story {

    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Id
    private Long id;
    private String title;

    @Column(columnDefinition = "TEXT")  // will be TEXT in db
//    @Column(length = 1000)
    private String content;
    private Date posted;

    @ManyToOne
    private Blogger blogger;

    // private noargs constructor for JPA
    private Story() {
    }

    public Story(String title, String content, Date posted, Blogger blogger) {
        this.title = title;
        this.content = content;
        this.posted = posted;
        this.blogger = blogger;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getPosted() {
        return posted;
    }

    public void setPosted(Date posted) {
        this.posted = posted;
    }

    public Blogger getBlogger() {
        return blogger;
    }

    public void setBlogger(Blogger blogger) {
        this.blogger = blogger;
    }

    @Override
    public String toString() {
        return "Story [title=" + title + "]";
    }

}