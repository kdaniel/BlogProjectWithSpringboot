package com.danielkocsis.springbootproject.controller;

import com.danielkocsis.springbootproject.service.StoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;

@Controller
public class HomeController {

//    @RequestMapping("/")
//    public String index() {
//        return "This runs over the stories.html in the static resources folder";
//    }

    private StoryService storyService;

    @Autowired
    public void setStoryService(StoryService storyService) {
        this.storyService = storyService;
    }

    @RequestMapping("/")
    public String stories(Model model, Locale locale) {
        // the model is a smuggler, it gives the value but nothing shows it, it looks like on the front end as it was static
        model.addAttribute("pageTitle", "One DanielKocsis Story per day");
        model.addAttribute("stories", storyService.getStories());
        System.out.println(String.format("Request received. Language: %s, Country: %s %n", locale.getLanguage(),
                locale.getDisplayCountry()));
        return "stories"; // resources/templates/stories.html
    }

    @RequestMapping("/story")
    public String story(Model model) {
        // the model is a smuggler, it gives the value but nothing shows it, it looks like on the front end as it was static
        model.addAttribute("pageTitle", "One DanielKocsis Story per day");
        model.addAttribute("story", storyService.getStory());

        return "story"; // resources/templates/story.html
    }

    @RequestMapping("/title/{title}")
    public String searchForStory(@PathVariable(value="title") String title, Model model) throws Exception{
        if (title == null) {
            throw new Exception("no such Story");
        }
        model.addAttribute("story", storyService.getSpecificStory(title));
        return "story";
    }


    @RequestMapping("/user/{id}")
    public String searchForUser(@PathVariable(value="id") String id) throws Exception{
        if (id == null) {
            throw new Exception("no such user with this id");
        }
        return "user";
    }

    @ExceptionHandler(Exception.class)
    public String exceptionHandler(HttpServletRequest ra, Exception ex, Model model) {
        model.addAttribute("errMessage", ex.getMessage());
        return "exceptionHandler";
    }

//    private List<Story> getStories() {
//
//        List<Story> stories = storyRepository.findAll();
//
//
////        Story story = new Story();
////        story.setTitle("first story");
////        story.setPosted(new Date());
////        story.setAuthor("DKocsis");
////        story.setContent("<p> Story time</p>");
////
////        Story story2 = new Story();
////        story2.setTitle("second story");
////        story2.setPosted(new Date());
////        story2.setAuthor("DKocsis");
////        story2.setContent("<p> Story time twice</p>");
////
////        stories.add(story);
////        stories.add(story2);
//
//        return stories;
//    }
}
